package platform

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"knife/files"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/golang/glog"
)

var macVerXML = "/System/Library/CoreServices/SystemVersion.plist"
var macVerKey = "ProductUserVisibleVersion"

// GetInstalledGoVersion -
func GetInstalledGoVersion() string {
	goVersion := ""
	goPath, OK := os.LookupEnv("GOROOT")
	if OK {
		versionFilePath := filepath.Join(goPath, "VERSION")
		if _, errMsg := os.Stat(versionFilePath); os.IsExist(errMsg) {
			bytesInFile, errMsg := ioutil.ReadFile(versionFilePath)
			if errMsg == nil {
				goVersion = string(bytesInFile)
			}
		}
	}

	return goVersion
}

func parsePlist(plistFile string) map[string]interface{} {
	xmlResult := map[string]interface{}{}
	xmlFile, errMsg := ioutil.ReadFile(plistFile)
	if errMsg != nil {
		fmt.Println("Argh!", errMsg)
	}
	xmlDecoder := xml.NewDecoder(strings.NewReader(string(xmlFile)))
	xmlDecoder.Strict = false
	var workingKey string
	for {
		xmlToken, _ := xmlDecoder.Token()
		if xmlToken == nil {
			break
		}
		switch startElem := xmlToken.(type) {
		case xml.StartElement:
			switch startElem.Name.Local {
			case "key":
				var keyName string
				errMsg := xmlDecoder.DecodeElement(&keyName, &startElem)
				if errMsg == nil {
					workingKey = keyName
				}
			case "string":
				var sValue string
				errMsg := xmlDecoder.DecodeElement(&sValue, &startElem)
				if errMsg == nil {
					xmlResult[workingKey] = sValue
				}
				workingKey = ""
			}
		}
	}
	return xmlResult
}

// GetMacVersion - Returns the macOS version
func GetMacVersion() string {
	plistDict := parsePlist(macVerXML)
	if _, OK := plistDict[macVerKey]; OK {
		return plistDict[macVerKey].(string)
	}
	return "0.0.0"
}

// GetCompiledGoVersion - Returns a string containing the version of Go used to compile the package.
func GetCompiledGoVersion() string {
	return runtime.Version()
}

// Architecture - Returns a string containing the arch of the current system.
func Architecture() string {
	return runtime.GOARCH
}

// Platform - Returns a string containing the os type of the current system.
func Platform() string {
	return runtime.GOOS
}

// Distro - Returns a strong containing the distro ( if applicable ) of the current system.
func Distro() string {
	uName := getUname()
	parseOsVersionFile("os-version")
	return uName
}

func getUname() string {
	return execCommand("uname -rs")
}

func getLSB() string {
	return execCommand("lsb_release -a")
}
func execCommand(exCmd string) string {
	cmdParts := strings.Fields(exCmd)
	cmdOut, cmdErr := exec.Command(cmdParts[0], cmdParts[1]).Output()
	if cmdErr != nil {
		fmt.Println("Encountered Error Executing Command ", cmdErr)
	}
	return strings.TrimSpace(string(cmdOut[:]))
}

func parseOsVersionFile(osVersionFile string) {
	osVersion := make(map[string]string)
	if doesExist := files.OSPathExists(osVersionFile); doesExist == true {
		fileConn, errMsg := os.Open(osVersionFile)
		if errMsg != nil {
			glog.Fatal(errMsg)
		}
		defer fileConn.Close()
		fileReader := bufio.NewScanner(fileConn)
		for fileReader.Scan() {
			keyValue := strings.Split(fileReader.Text(), "=")
			osVersion[keyValue[0]] = keyValue[1]
		}
	}
}
